import 'package:envied/envied.dart';
part 'env.g.dart';

@Envied(path: "lib/env/apiKey.env")
abstract class Env {
  @EnviedField(varName: 'OPEN_AI_API_KEY')
  static const String API_KEY = _Env.API_KEY;
}
