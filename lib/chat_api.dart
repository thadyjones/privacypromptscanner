import 'package:dart_openai/openai.dart';
import 'package:privacy_prompt_scanner/env/env.dart';

class ChatApi {
  static const _model = 'gpt-3.5-turbo';

  final systemMessage = const OpenAIChatCompletionChoiceMessageModel(
      role: OpenAIChatMessageRole.assistant,
      content:
          "Suround any privacy sensitive infotmation like names, addresses or telefone numbers with the String. \"\$\$privacy concern\$\$\" in the following text.");

  ChatApi() {
    OpenAI.apiKey = Env.API_KEY;
    OpenAI.organization = null;
  }

  Future<String> getPrivacyConcerns(String text) async {
    var userMessage = OpenAIChatCompletionChoiceMessageModel(
        role: OpenAIChatMessageRole.user, content: text);

    final requestMessage = [systemMessage, userMessage];

    OpenAIChatCompletionModel chatCompletion = await OpenAI.instance.chat
        .create(model: _model, messages: requestMessage, temperature: 0.2);

    return chatCompletion.choices.first.message.content;
  }
}
