import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'prompt_highlighting.dart';
import 'dart:math';

void main() {
  runApp(const PrivacyPromptScannerApp());
}

class PrivacyPromptScannerApp extends StatelessWidget {
  const PrivacyPromptScannerApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => PromptAppState(),
      child: const MaterialApp(
        home: PromptInput(),
      ),
    );
  }
}

class PromptAppState extends ChangeNotifier {
  final personalColor = Color.fromRGBO(255, 0, 0, 1);
  final sensitiveColor = Color.fromRGBO(254, 165, 0, 1);
  var allIssues = [];
  bool loading = false;
  final GlobalKey<_CustomProgressIndicatorState> progressKey = GlobalKey();

  final _alternativeClient =
      AlternativesClient('http://localhost:8080/alternative');

  String prompt = '';
  Map<String, dynamic> issues = {"personal": [], "sensitive": []};
  List<TextSpan> analysedPrompt = [const TextSpan(text: "")];
  num nTotalIssues = 0;
  num nRemainingIssues = 0;

  void updatePrompt(String newPrompt) {
    prompt = newPrompt;
    notifyListeners();
  }

  void updateIssues(Map<String, dynamic> newIssues) {
    issues = newIssues;
    notifyListeners();
  }

  void setNTotalIssues(num numberIssues) {
    nTotalIssues = numberIssues;
    notifyListeners();
  }

  void createAnalysedPrompt(BuildContext topContext) {
    var textSpans = [];
    allIssues.clear();
    nRemainingIssues = 0;
    for (var key in issues.keys) {
      nRemainingIssues += issues[key].length;
      Color textColor;
      if (key == "personal") {
        textColor = personalColor;
      } else if (key == "sensitive") {
        textColor = sensitiveColor;
      } else {
        textColor = const Color.fromRGBO(0, 0, 0, 1.0);
      }
      Map issuesMap = issues[key].asMap();
      for (var i in issuesMap.keys) {
        String text = issuesMap[i][2];
        int start = issuesMap[i][0];
        int end = issuesMap[i][1];
        allIssues.add([key, i, start, end, text]);
        TextEditingController textAlertEditingController =
            TextEditingController();
        late TapGestureRecognizer gestureRecognizer = TapGestureRecognizer();
        textSpans.add([
          start,
          end,
          TextSpan(
              text: text,
              style: TextStyle(color: textColor),
              recognizer: gestureRecognizer
                ..onTap = () async {
                  await showDialog(
                      context: topContext,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text("Change: $text"),
                          content: TextField(
                            controller: textAlertEditingController,
                            decoration: const InputDecoration(
                                hintText: "Input changes hier..."),
                          ),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: const Text("CANCEL")),
                            TextButton(
                                onPressed: () async {
                                  Navigator.pop(context);
                                  Future<String> alternative =
                                      getAlternatives(text);
                                  alternative.then((value) => changeIssue(
                                      key, i, start, end, text, value));
                                  await showDialog(
                                      context: context,
                                      builder: (context) =>
                                          FutureProgressDialog(alternative,
                                              message: const Text(
                                                  "Waiting for server...")));
                                  createAnalysedPrompt(topContext);
                                },
                                child: const Text("AUTO")),
                            TextButton(
                                onPressed: () {
                                  changeIssue(key, i, start, end, text,
                                      textAlertEditingController.text);
                                  createAnalysedPrompt(topContext);
                                  Navigator.pop(context);
                                },
                                child: const Text("OK"))
                          ],
                        );
                      });
                })
        ]);
      }
    }

    textSpans.sort((a, b) => a[0] - b[0]);
    List<List> uniqueSpans = [];

    for (var span in textSpans) {
      if (uniqueSpans.isEmpty || uniqueSpans.last[1] <= span[0]) {
        uniqueSpans.add(span);
      }
    }
    textSpans = uniqueSpans;
    var finalTextSpans = <TextSpan>[];
    var currentSupString = prompt;
    var cutIndex = 0;
    print(textSpans);
    for (var textSpan in textSpans) {
      print(currentSupString);
      print(textSpan);
      print(textSpan[0]);
      print(cutIndex);

      finalTextSpans.add(TextSpan(
          text: currentSupString.substring(0, textSpan[0] - cutIndex)));
      finalTextSpans.add(textSpan[2]);
      currentSupString = currentSupString.substring(textSpan[1] - cutIndex);
      cutIndex = textSpan[1];
    }
    finalTextSpans.add(TextSpan(text: currentSupString));
    analysedPrompt = finalTextSpans;
    notifyListeners();
  }

  void updateAnalysedPrompt(List<TextSpan> newAnalysedPrompt) {
    analysedPrompt = newAnalysedPrompt;
  }

  void changeIssue(String key, int index, int start, int end, String oldText,
      String newText) {
    int newIssueLength = newText.length;
    int removedIssueLength = end - start;
    int shiftLength = newIssueLength - removedIssueLength;

    var splitPrompt = prompt.split(oldText);
    updatePrompt(splitPrompt[0] + newText + splitPrompt[1]);

    var newIssues = issues;
    newIssues[key].removeAt(index);
    for (var k in newIssues.keys) {
      for (var index = 0; index < newIssues[k].length; index++) {
        if (newIssues[k][index][0] >= end) {
          newIssues[k][index][0] += shiftLength;
          newIssues[k][index][1] += shiftLength;
        }
      }
    }
    updateIssues(newIssues);
  }

  Future<String> getAlternatives(String issue) async {
    try {
      return await _alternativeClient.postData(issue);
    } catch (e) {
      print('Error in alternativesClient: $e');
      return "";
    }
  }
}

class PromptInput extends StatefulWidget {
  const PromptInput({super.key});

  @override
  State<PromptInput> createState() => _PromptInputState();
}

class _PromptInputState extends State<PromptInput> {
  late TextEditingController _inputController;
  late FocusNode _focusNode;

  final _highlightingClient =
      PromptHighlightingClient('http://localhost:8080/highlight');

  @override
  void initState() {
    super.initState();
    _inputController = TextEditingController();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    _inputController.dispose();
    super.dispose();
  }

  Future<Map<String, dynamic>> analysePrompt(String prompt) async {
    try {
      return await _highlightingClient.postData(prompt);
    } catch (e) {
      print('Error in highlightingClient: $e');
      return {"personal": [], "sensitive": []};
    }
  }

  Color getNIssuesColor(PromptAppState appState) {
    if (appState.nRemainingIssues == 0) {
      return Colors.green;
    } else if (appState.nRemainingIssues < appState.nTotalIssues * 0.5) {
      return Colors.orange;
    } else {
      return Colors.red;
    }
  }

  @override
  Widget build(BuildContext topContext) {
    var appState = topContext.watch<PromptAppState>();
    _inputController
        .addListener(() => appState.updatePrompt(_inputController.text));
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Center(
                child: TextField(
                  inputFormatters: [
                    CustomTextInputFormatter(),
                  ],
              controller: _inputController,
              decoration: const InputDecoration(
                  border: OutlineInputBorder(), labelText: 'Prompt'),
              onChanged: (String value) {
                setState(() {});
              },
            )),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: ElevatedButton(
                      onPressed: () async {
                        await showDialog(
                            context: topContext,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: const Text('Submit Prompt?'),
                                actions: [
                                  TextButton(
                                      onPressed: () => Navigator.pop(context),
                                      child: const Text("CANCEL")),
                                  TextButton(
                                      onPressed: () async {
                                        appState.updatePrompt(
                                            _inputController.text);
                                        Navigator.pop(context);
                                        print(appState.prompt.characters);
                                        Future<Map<String, dynamic>> issues =
                                            analysePrompt(appState.prompt);
                                        issues.then((value) {
                                          num nIssues = 0;
                                          for (var key in value.keys) {
                                            nIssues += value[key].length;
                                          }
                                          appState.setNTotalIssues(nIssues);
                                          appState.updateIssues(value);
                                          appState
                                              .createAnalysedPrompt(topContext);
                                          setState(() {});
                                        });
                                        await showDialog(
                                            context: context,
                                            builder: (context) =>
                                                FutureProgressDialog(issues,
                                                    message: const Text(
                                                        "Waiting for Server ...")));
                                      },
                                      child: const Text('OK'))
                                ],
                              );
                            });
                      },
                      child: const Text('Analyse')),
                ),
              ],
            ),
            Expanded(
              child: Container(
                width: double.maxFinite,
                decoration: BoxDecoration(
                  border: Border.all(width: 0.5),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SelectableRegion(
                      selectionControls: materialTextSelectionControls,
                      focusNode: _focusNode,
                      child: Text.rich(TextSpan(
                          children: appState.analysedPrompt,
                          style: const TextStyle(color: Colors.black))),
                    )),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            children: [
                              CustomProgressIndicator(
                                  key: appState.progressKey),
                              ElevatedButton(
                                  child: const Text("Auto Replace All"),
                                  onPressed: () async {
                                    appState.progressKey.currentState
                                        ?.updateProgress(0.1);
                                    print(appState.allIssues);
                                    appState.allIssues
                                        .sort((a, b) => a[3] - b[3]);

                                    var _issues = appState.allIssues;
                                    for (var idx = _issues.length - 1;
                                        idx > -1;
                                        idx--) {
                                      var issue = _issues[idx];
                                      print(
                                          "get alternatives for: " + issue[4]);
                                      String alternative = await appState
                                          .getAlternatives(issue[4]);
                                      print("alternative: " + alternative);
                                      appState.changeIssue(
                                          issue[0],
                                          issue[1],
                                          issue[2],
                                          issue[3],
                                          issue[4],
                                          alternative);
                                      appState.createAnalysedPrompt(context);
                                    }
                                    appState.progressKey.currentState
                                        ?.updateProgress(0.0);

                                    print("replaced all");
                                  }),
                            ],
                          ),
                        ),
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ElevatedButton(
                              onPressed: () async {
                                await Clipboard.setData(
                                    ClipboardData(text: appState.prompt));
                                await showDialog(
                                    context: topContext,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        content:
                                            const Text("Copied to Clipboard!"),
                                      );
                                    });
                              },
                              child: const Text("Copy"),
                            )),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text.rich(
                        TextSpan(children: [
                          const TextSpan(text: "Number of "),
                          TextSpan(
                              text:
                                  "remaining issues: ${appState.nRemainingIssues}",
                              style:
                                  TextStyle(color: getNIssuesColor(appState))),
                          const TextSpan(text: " / "),
                          TextSpan(
                              text: "total issues: ${appState.nTotalIssues}")
                        ]),
                        style: TextStyle(
                            color: appState.nTotalIssues > 10
                                ? Colors.red
                                : Colors.black),
                      ),
                    )
                  ],
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(width: 0.5),
                      borderRadius: BorderRadius.circular(5)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text.rich(
                      TextSpan(children: [
                        const TextSpan(text: "Legend: \n"),
                        TextSpan(
                            text: "\t Personal",
                            style: TextStyle(color: appState.personalColor)),
                        const TextSpan(text: " privacy issues\n"),
                        TextSpan(
                            text: "\t Sensitive",
                            style: TextStyle(color: appState.sensitiveColor)),
                        const TextSpan(text: " privacy issues")
                      ]),
                      style: const TextStyle(color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: const Text('Privacy Prompt Scanner'),
      ),
    );
  }
}

class CustomProgressIndicator extends StatefulWidget {
  CustomProgressIndicator({required Key key}) : super(key: key);

  @override
  _CustomProgressIndicatorState createState() =>
      _CustomProgressIndicatorState();
}

class _CustomProgressIndicatorState extends State<CustomProgressIndicator> {
  double progress = 0.0;

  void updateProgress(double newProgress) {
    setState(() {
      progress = newProgress;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (progress == 0.0) {
      return Container();
    } else {
      return Container(
          child: CircularProgressIndicator(),
          margin: EdgeInsets.only(right: 10));
    }
  }
}

class CustomTextInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    String newText = newValue.text;

    // Allow only specific characters and replace multiple spaces with a single space
    newText = newText.replaceAll(RegExp(r'[^a-zA-Z ,.?!/]'), '')
        .replaceAll(RegExp(r' +'), ' ');

    return TextEditingValue(
      text: newText,
      selection: newValue.selection.copyWith(
        baseOffset: min(newText.length, newValue.selection.baseOffset),
        extentOffset: min(newText.length, newValue.selection.extentOffset),
      ),
    );
  }
}
