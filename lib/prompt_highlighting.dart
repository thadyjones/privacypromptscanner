import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class PromptHighlightingClient {
  final String apiUrl;

  PromptHighlightingClient(this.apiUrl);

  Future<Map<String, dynamic>> postData(String text) async {
    Map<String, dynamic> jsonResponse = {"personal": null, "sensitive": null};
    try {
      final response = await http.post(
        Uri.parse(apiUrl),
        headers: {'Content-Type': 'application/json'},
        body: '{"text":"$text"}',
      );

      if (response.statusCode == 200) {
        jsonResponse =
            convert.jsonDecode(response.body) as Map<String, dynamic>;
      } else {
        print('Failed with status code ${response.statusCode}.');
      }
    } catch (e) {
      print('Error: $e');
    }
    return jsonResponse;
  }
}

class AlternativesClient {
  final String apiUrl;

  AlternativesClient(this.apiUrl);

  Future<String> postData(String text) async {
    Map<String, dynamic> jsonResponse = {"alternative": null};
    try {
      final response = await http.post(
        Uri.parse(apiUrl),
        headers: {'Content-Type': 'application/json'},
        body: '{"text":"$text"}',
      );

      if (response.statusCode == 200) {
        jsonResponse =
            convert.jsonDecode(response.body) as Map<String, dynamic>;
      } else {
        print('Failed with status code ${response.statusCode}.');
      }
    } catch (e) {
      print('Error: $e');
    }
    return jsonResponse["alternative"];
  }
}
