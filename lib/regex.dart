class NameFinder {
  static const names = <String>['Johannes Windson', 'Birgit König'];

  NameFinder();

  List<String> splitAtAllNames(String text) {
    var splitText = <String>[];

    for (var name in names) {
      for (var split in text.split(name)) {
        splitText.add(split);
        splitText.add(name);
      }
      splitText.removeLast();
    }
    return splitText;
  }
}
