# privacy_prompt_scanner
Privacy prompt scanner (PPS) is a tool, designed to check LLM-prompts for privacy concerns.

## Installation
Download the **release** Folder. Currently the bundled release only supports Windows. PPS can be started by executing the **privacy_prompt_scanner.exe** executable.

PPS requires that a **prompt-highlighting-server** is running in the background. You can find it here: "https://git.informatik.uni-hamburg.de/8michael/prompt-highlighting-server".
Simply clone the repository and start the server using the provieded starting arguements. A conda environment file is provided in the release.

## Functionality
PPS scannes your prompt for privacy concerns. Simply paste the prompt into the text field at the top and click *Analyse*.
The application will then analyse the prompt and display it, with the detected text highlighted.

> PPS discriminates between two kinds of privacy concern:
> 1. *personal*: Marked in red, these issues concerne the privacy of indivuals and include: names, mentioning of deseases, ...
> 2. *sensitive*: Issues marked in orange are file paths or organizational structures.

You can change each indivdual marked text by clicking on it and either giving PPS a alternative, or to let an LLM change it for you. There is also the option to automaticaly replace all highlighted text. 
Once done, you can either highlight and copy the changed prompt, or press the *Copy*-button to add it to your clipboard.

## How it works
PPS was build using FLutter and analyses the prompt by calling the python backend, which in turn interfaces with a LLM based in the university. PPS then uses the returned issues to create a textfield where the original prompt is displayed, with the issues highlighted. It achieves this by splitting the orignal prompt at the issues and replacing them with the highlighted text. 
When changing a marked text, the corresponding issue is removed from the list and the prompt adjusted. Then the whole textfield is rebuild.
For the *auto-replace*-function, PPS requests an alternative from the backend, which provieds by asking a LLM.

## Bugs and Issues
- Auto-replace functions take long, since the used LLM is slow
- Sometimes the alternative given by the LLM is not good and can't be changed